

import UIKit
// модель строки чата
class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageTextLabel: UILabel! // текст сообщения
    @IBOutlet weak var senderNameLabel: UILabel! // отправитель сообщения
    // заполнить строку данными из Message
    func fillData(from message: Message) {
        messageTextLabel.text = message.text
        senderNameLabel.text = message.sender
    }

}
