

import UIKit
// экран с чатом пользователей
class ChatViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! // аутлет таблицы
    @IBOutlet weak var messageTF: UITextField! // поле ввода сообщения
    @IBOutlet private weak var bottomViewHeightConstraint: NSLayoutConstraint! // констрейнт textField

    // MARK: - Properties
    // userName, введенный при авторизации
    var userName: String!
    // обработчик сокетов
    let socketLoader = Loaders.socket
    // массив сообщений пользователей
    var messages: [Message] = [] {
        didSet {
            tableView.reloadData() // обновляем таблицу
            tableView.rowHeight = UITableView.automaticDimension // динамическая высота строки
            tableView.scrollToDown() // скроллим таблицу вниз для показа новых сообщений
        }
    }
    // MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        // сдвигаем вью для вывода на экран клавиатуры
        setupKeyboardNotifications()
        // выводим логин в тайтл navigationBar
        self.navigationItem.title = userName
        // отображаем сообщения в real-time
        startObservingMessages()
    }
    
    // MARK: - Observe messages
    func startObservingMessages() {
        socketLoader.observeMessages(completionHandler: { [weak self] data in
            // парсим текст сообщения по ответу с сервера
            let name = data["nickname"] as! String
            let text = data["message"] as! String
            // инициализируем объект Message
            let message = Message(text: text, sender: name)
            // наполняем массив
            self?.messages.append(message)
        })
    }
    // MARK: - Keyboard funcs
    // функции сдвига клавиатуры
    private func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(with:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(with:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc
    private func keyboardWillShow(with notification: Notification) {
        guard let info = notification.userInfo, let keyboardEndSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size else {
            return
        }
        
        let keyboardHeight = keyboardEndSize.height
        
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.bottomViewHeightConstraint.constant = keyboardHeight
            
            self?.view.layoutIfNeeded()
        }
    }
    
    @objc
    private func keyboardWillHide(with notification: Notification) {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.bottomViewHeightConstraint.constant = 0
            
            self?.view.layoutIfNeeded()
        }
    }
    // MARK: - Send a message
    // кнопка отправления сообщения
    @IBAction func sendMessage(_ sender: Any) {
        guard let text = messageTF.text else { return  }
        // посылаем сокет с текстом сообщения
        socketLoader.send(message: text, username: userName)
        messageTF.text = "" // очищаем textField
    }
}
// MARK: - tableView dataSource
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        messages.count // количество сообщения
    }
    // инициализируем контент строки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageTableViewCell
        cell.fillData(from: message)
        return cell
    }
    
}
// MARK: - tableView scroll to down
// функция скролла таблицы до самой нижней строки
extension UITableView {
    func scrollToDown() {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
        }
    }
}
