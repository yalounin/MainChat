

import UIKit
// модель строки экрана со списком пользователей
class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel! // имя пользователя
    @IBOutlet weak var statusLabel: UILabel! // статус пользователя
    // заполняем данными из User
    func fillData(from user: User) {
        userNameLabel.text = user.userName
        statusLabel.text = user.isOnline ? "online" : "offline"
    }

}
