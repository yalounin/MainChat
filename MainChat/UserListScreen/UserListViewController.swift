

import UIKit
// экран со списком пользователей
class UserListViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Properties
    // список пользователей
    var users: [User] = [] {
        didSet {
            tableView.reloadData() // обновляем таблицу при изменении
            tableView.rowHeight = UITableView.automaticDimension // динамическая высота строки
        }
    }
    // обработчик сокетов (тот же, что и в предыдущем VC)
    let socketLoader = Loaders.socket
    // userName, введенный при авторизации
    var userName: String!

    // MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()

        connectToChat() // присоединяемся к списку пользователей
        startObservingUserList() // обновляем в real-time список пользователей
    }
    // MARK: - Connect to the chat
    private func connectToChat() {
        socketLoader.connectToChat(with: self.userName)
    }
    // MARK: - Observe User List
    private func startObservingUserList() {

        socketLoader.observeUserList(completionHandler: { [weak self] data in

            var currentUsers: [User] = []
            // инициализируем модель User для данных, полученных из сокетов
            for userData in data {
                let name = userData["nickname"] as! String
                let isConnected = userData["isConnected"] as! Bool
                
                let user = User(userName: name, isOnline: isConnected)
                
                currentUsers.append(user)
            }
            self?.users = currentUsers
        })
    }
    // MARK: - Segue init
    // подготавливаем данные для перехода в окно чата
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChat" {
            guard let userName = sender as? String else {
                return
            }
            
            guard let destinationViewController = segue.destination as? ChatViewController else {
                return
            }
            // передаем userName
            destinationViewController.userName = userName
    }
    }
    // MARK: - Button action
    // по нажатию кнопки на navigationBar открываем окно чата
    @IBAction func joinChat(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "ShowChat", sender: self.userName)
    }
}

// MARK: - tableView dataSource
extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        users.count // количество строк по количеству юзеров
    }
    // инициализируем строки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = users[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserTableViewCell
        
        cell.fillData(from: user)
        
        return cell
    }
}
