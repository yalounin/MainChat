
import Foundation
import SocketIO
// класс для инициализации обработки сокетов
class SocketLoader: NSObject {
    // основные properties
    private var manager: SocketManager!
    private var socket: SocketIOClient!
        
    // инициализатор
    override init() {
        super.init()
        // инициализируем сокет-менеджер с ip-адресом сервера
        manager = SocketManager(socketURL: URL(string: "http://192.168.245.71:3000")!)
        socket = manager.defaultSocket
    }
    // установить соединение
    func establishConnection() {
        socket.connect()
    }
    // прервать соединение
    func closeConnection() {
        socket.disconnect()
    }
    // присоединиться к списку юзеров
    func connectToChat(with name: String) {
        socket.emit("connectUser", name)
    }
    // "слушать" изменения списка юзеров
    func observeUserList(completionHandler: @escaping ([[String: Any]]) -> Void) {
        socket.on("userList") { dataArray, _ in

            completionHandler(dataArray[0] as! [[String: Any]])
        }
    }
    // отправить новое сообщение
    func send(message: String, username: String) {
        socket.emit("chatMessage", username, message)
    }
    // "слушать" изменения списка сообщений
    func observeMessages(completionHandler: @escaping ([String: Any]) -> Void) {
        socket.on("newChatMessage") { dataArray, _ in
            var messageDict: [String: Any] = [:]
            
            messageDict["nickname"] = dataArray[0] as! String
            messageDict["message"] = dataArray[1] as! String
            
            completionHandler(messageDict)
        }
    }
}
