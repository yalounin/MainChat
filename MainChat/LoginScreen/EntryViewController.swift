

import UIKit

// экран авторизации
class EntryViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField! // окно ввода логина
    
    // MARK: - Properties
    // загрузчик сокетов
    let socketLoader = Loaders.socket
    
    // MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // скрыть клавиатуру при тапе по вью
        view.addGestureRecognizer(UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:))))

        socketLoader.establishConnection() // устанавливаем соединение через статическую переменную
    }
    
    // MARK: - Segue init
    // открываем окно с именами юзеров
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowUserList" {
            guard let userName = sender as? String else {
                return
            }
            guard let destinationViewController = segue.destination as? UserListViewController else {
                return
            }
            // передаем userName, введенный при авторизации
            destinationViewController.userName = userName
    }
    }
    // MARK: - Button action
    // кнопка ввода логина
    @IBAction func enterUserName(_ sender: Any) {
        let userName = userNameTextField.text
        self.performSegue(withIdentifier: "ShowUserList", sender: userName)
    }
    

}
