Проект: "Main Chat"

Цель: Обмен сообщениями между пользователями в одном общем чате

Приложение состоит из 3-х экранов:
1. Экран авторизации - ввод имени пользователя
2. Экран списка юзеров, обновляемый в real-time
3. Экран общего чата, обновляемый в real-time

Stack:
Swift
UIKit
SocketIO

Server: https://github.com/appcoda/SocketIOChat/blob/master/srv-SocketChat.zip 
Architecture: Model-View-Controller
